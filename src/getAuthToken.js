import { singlePromise } from './util'

const secondISOStr = expiresIn => {
  const sec = expiresIn || (process.env.NODE_ENV === 'production' ? 3600 : 10)
  // 10 secs before real expire
  return new Date(Date.now() + (sec - 10) * 1000).toISOString()
}
export const calcAuthState = ({ accessToken, refreshToken, expireAt, expiresIn, username } = {}) => {
  return {
    accessToken,
    refreshToken,
    username,
    expiresIn,
    expireAt: expireAt || (refreshToken ? secondISOStr(expiresIn) : null),
  }
}

const nowIso = () => new Date(Date.now()).toISOString()

export const getAuthToken = singlePromise(({ getAuthState, fetchRefreshToken, setAuthState }) => {
  const { accessToken, expireAt, refreshToken, username, expiresIn } = getAuthState()

  if (accessToken && (!expireAt || nowIso() < expireAt)) {
    return Promise.resolve(accessToken)
  }

  if (!refreshToken || !fetchRefreshToken || !setAuthState) {
    return Promise.resolve(null)
  }

  return fetchRefreshToken({ refreshToken, username, accessToken, expireAt, expiresIn })
    .then(async refreshed => {
      if (refreshed.accessToken) {
        await setAuthState(calcAuthState({ ...refreshed, refreshToken: refreshed.refreshToken || refreshToken }))
      }
      return refreshed.accessToken
    })
    .catch(() => accessToken)
})
