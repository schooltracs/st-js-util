export const parseJson = str => {
  try {
    return JSON.parse(str)
  } catch (err) {
    return undefined
  }
}

export const singlePromise = func => {
  let p = null
  return (...args) => {
    if (p) return p
    p = Promise.resolve(func(...args)).finally(() => {
      p = null
    })
    return p
  }
}
