# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.15](https://bitbucket.org/schooltracs/js-util/compare/v1.1.14...v1.1.15) (2020-04-15)


### Bug Fixes

* move app base react comp here ([f034eb8](https://bitbucket.org/schooltracs/js-util/commit/f034eb8))

### [1.1.14](https://bitbucket.org/schooltracs/js-util/compare/v1.1.13...v1.1.14) (2019-09-24)


### Bug Fixes

* pass all auth state to fetchRefreshToken ([cceb13f](https://bitbucket.org/schooltracs/js-util/commit/cceb13f))

### [1.1.13](https://bitbucket.org/schooltracs/js-util/compare/v1.1.12...v1.1.13) (2019-09-20)


### Bug Fixes

* expireAt calc ([928006a](https://bitbucket.org/schooltracs/js-util/commit/928006a))

### [1.1.12](https://bitbucket.org/schooltracs/js-util/compare/v1.1.11...v1.1.12) (2019-09-19)


### Bug Fixes

* null expireAt means always expired ([c735a48](https://bitbucket.org/schooltracs/js-util/commit/c735a48))

### [1.1.11](https://bitbucket.org/schooltracs/js-util/compare/v1.1.10...v1.1.11) (2019-09-18)


### Bug Fixes

* getAuthToken, check accessToken and expireAt ([76bce76](https://bitbucket.org/schooltracs/js-util/commit/76bce76))

### [1.1.10](https://bitbucket.org/schooltracs/js-util/compare/v1.1.9...v1.1.10) (2019-08-19)


### Bug Fixes

* back to old eslint ([c02c8a5](https://bitbucket.org/schooltracs/js-util/commit/c02c8a5))



### [1.1.9](https://bitbucket.org/schooltracs/js-util/compare/v1.1.8...v1.1.9) (2019-08-12)



### [1.1.8](https://bitbucket.org/schooltracs/js-util/compare/v1.1.7...v1.1.8) (2019-08-12)



### [1.1.7](https://bitbucket.org/schooltracs/js-util/compare/v1.1.6...v1.1.7) (2019-07-22)


### Bug Fixes

* return accessToken if no fetchRefreshToken ([0e7dd63](https://bitbucket.org/schooltracs/js-util/commit/0e7dd63))



### [1.1.6](https://bitbucket.org/schooltracs/js-util/compare/v1.1.5...v1.1.6) (2019-07-22)


### Bug Fixes

* add username to login and refresh token ([5025d16](https://bitbucket.org/schooltracs/js-util/commit/5025d16))



### [1.1.5](https://bitbucket.org/schooltracs/js-util/compare/v1.1.4...v1.1.5) (2019-07-10)


### Bug Fixes

* ensure await setAuthState ([f3ec9b7](https://bitbucket.org/schooltracs/js-util/commit/f3ec9b7))



### [1.1.4](https://bitbucket.org/schooltracs/js-util/compare/v1.1.3...v1.1.4) (2019-07-10)


### Bug Fixes

* refactor auth and jsonFetch ([29f0e4c](https://bitbucket.org/schooltracs/js-util/commit/29f0e4c))



### [1.1.3](https://bitbucket.org/schooltracs/js-util/compare/v1.1.2...v1.1.3) (2019-07-09)


### Bug Fixes

* try to fix refresh token problem ([361f1b2](https://bitbucket.org/schooltracs/js-util/commit/361f1b2))



### [1.1.2](https://bitbucket.org/schooltracs/js-util/compare/v1.1.1...v1.1.2) (2019-07-04)


### Bug Fixes

* enhance makeAccessTokenAdder ([29d0300](https://bitbucket.org/schooltracs/js-util/commit/29d0300))



### [1.1.1](https://bitbucket.org/schooltracs/js-util/compare/v1.1.0...v1.1.1) (2019-07-04)


### Bug Fixes

* makeAccessTokenAdder and eslint ([4b7fbdb](https://bitbucket.org/schooltracs/js-util/commit/4b7fbdb))



## [1.1.0](https://bitbucket.org/schooltracs/js-util/compare/v1.0.6...v1.1.0) (2019-07-02)


### Features

* add getAuthToken ([ee928dc](https://bitbucket.org/schooltracs/js-util/commit/ee928dc))



### [1.0.6](https://bitbucket.org/schooltracs/js-util/compare/v1.0.5...v1.0.6) (2019-06-26)


### Bug Fixes

* rename makeXXXFetch to fetchWithXXX ([1dc61e1](https://bitbucket.org/schooltracs/js-util/commit/1dc61e1))



### [1.0.5](https://bitbucket.org/schooltracs/js-util/compare/v1.0.4...v1.0.5) (2019-06-25)


### Bug Fixes

* refactor fetches ([3685443](https://bitbucket.org/schooltracs/js-util/commit/3685443))



### [1.0.4](https://bitbucket.org/schooltracs/js-util/compare/v1.0.3...v1.0.4) (2019-06-24)


### Bug Fixes

* refactor fetches ([341f755](https://bitbucket.org/schooltracs/js-util/commit/341f755))



## [1.0.3](https://bitbucket.org/schooltracs/js-util/compare/v1.0.2...v1.0.3) (2019-06-13)



## [1.0.2](https://bitbucket.org/schooltracs/js-util/compare/v1.0.1...v1.0.2) (2019-06-12)



## 1.0.1 (2019-06-12)


### Bug Fixes

* add makeFetch and eslintrc ([6f268f6](https://bitbucket.org/schooltracs/js-util/commits/6f268f6))
