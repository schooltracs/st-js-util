import Querystring from 'querystring'
import _ from 'lodash'
import { createBrowserHistory } from 'history'
import { useLocation } from 'react-router-dom'

/*
 * We are moving this to st-js-util
 * Please DON'T add ANY project spec code into here OR hack in here
 */

// ---------------------------------------------------------------------
// queryString util
// ---------------------------------------------------------------------

let compressKeysTable
let decompressKeysTable

const isArrayKey = key => key.endsWith('Ids') || key.endsWith('Arr')

const stringifyValue = (value, key) => {
  if (Array.isArray(value)) {
    const v = _.compact(value)
    if (v.length === 0) {
      return undefined
    }
    if (!isArrayKey(key)) {
      if (process.env.NODE_ENV === 'development') {
        console.warn(`Deprecated! Please change query-string key ${key} to end with 'Ids' or 'Arr'`)
      }
      return `*${v.join('*')}`
    }
    return v.join('*')
  }
  // Querystring cannot store null, so treat null and '' as same thing
  if (value === null) return ''
  return value
}

const _parseVal = (v, isId) => {
  if (v === 'true') return true
  if (v === 'false') return false
  if (!isId && !isNaN(v)) {
    // console.warn(`Deprecated! value ${v} parseFloat, which is too magic`)
    return parseFloat(v)
  }
  return v
}
const getIsIdKey = k => k.match(/ids?$/i)
const parseValue = (value, key) => {
  const isId = getIsIdKey(key)
  if (value.charAt(0) === '*') {
    return value
      .substring(1)
      .split('*')
      .map(subV => _parseVal(subV, isId))
  }
  if (isArrayKey(key)) {
    return value.split('*').map(subV => _parseVal(subV, isId))
  }
  return _parseVal(value, isId)
}

export const stringifyQueryString = q => {
  const qObj = _.transform(q, (ret, value, k) => {
    if (k === 'pathname' || k === 'centerId' || k === '_make' || k === '_merge') return
    const v = stringifyValue(value, k)
    if (v === undefined) return
    ret[compressKeysTable[k] || k] = v
  })
  return Querystring.stringify(qObj)
}

export const parseQueryString = s => {
  if (!s) return {}
  const str = s && s.charAt(0) === '?' ? s.substring(1) : s
  return _.transform(Querystring.parse(str), (ret, value, k) => {
    const qk = decompressKeysTable[k] || k
    ret[qk] = parseValue(value, qk)
  })
}

// ---------------------------------------------------------------------
// queryString util END
// ---------------------------------------------------------------------

// config queryString
let _createHistory = createBrowserHistory
let _postParse = _.identity
let _preStringify = _.identity
let _onCreated = _.identity
export const configAppHistory = ({ compressKeys, createHistory, postParse, preStringify, onCreated }) => {
  if (compressKeys) {
    compressKeysTable = compressKeys
    decompressKeysTable = _.invert(compressKeys)
  }
  if (createHistory) _createHistory = createHistory
  if (postParse) _postParse = postParse
  if (preStringify) _preStringify = preStringify
  if (onCreated) _onCreated = onCreated
}

configAppHistory({
  // defaults for both st4 and st-report
  compressKeys: {
    orgId: 'o',
    centerIds: 'c',
    date: 'd',
    endDate: 'ed',
    customerIds: 'ss',
    orderIds: 'os',
    ttDate: 'td',
    activeActivityId: 'aa',
    feeId: 'f',
    activeOrderId: 'ao',
    tSearch: 'ts',
    tLines: 'tl',
  },
})

const hackLocation = location => {
  const query = parseQueryString(location.search)
  query.pathname = location.pathname
  location.query = _postParse(query, location)
}

const createAppHistory = () => {
  const appHistory = _createHistory()

  // hack history
  const oldPush = appHistory.push.bind(appHistory)
  const oldReplace = appHistory.replace.bind(appHistory)

  Object.assign(appHistory, {
    make: loc => {
      // not support loc is string
      if (loc._made) {
        delete loc._made
        return loc
      }
      const oldLoc = appHistory.location
      const pathname = `${loc.pathname || oldLoc.pathname}`
      // always-keeps: orgId, centerIds (omits centerId?)   config-keeps: 'date', 'endDate'
      const oldQuery = loc._merge ? oldLoc.query : _.pick(oldLoc.query, 'orgId', 'centerIds', 'date', 'endDate')
      const newQuery = { ...oldQuery, ...(loc.query || loc) }
      return {
        pathname,
        query: newQuery,
        search: stringifyQueryString(_preStringify(newQuery, oldLoc)),
        _made: true,
      }
    },

    createHref: _.wrap(appHistory.createHref, (createHref, loc) => {
      return createHref(appHistory.make(loc))
    }),

    push: (loc, state) => oldPush(appHistory.make(loc), state),
    replace: (loc, state) => oldReplace(appHistory.make(loc), state),

    // deprecate and use push() directly?
    mergePush: (loc, state) => appHistory.push({ ...loc, _merge: true }, state),
    mergeReplace: (loc, state) => appHistory.replace({ ...loc, _merge: true }, state),
  })

  // hack location
  appHistory.listen(hackLocation)
  hackLocation(appHistory.location)
  _onCreated(appHistory)
  return appHistory
}

const getAppHistory = () => {
  if (global.appHistory && !_.isEmpty(global.appHistory.location.query.centerIds)) {
    return global.appHistory
  }
  return (global.appHistory = createAppHistory())
}

export default getAppHistory

export const useLocQuery = defaults => {
  const query = useLocation().query
  if (defaults) _.defaults(query, defaults)
  return query
}
