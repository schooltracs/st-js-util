import React from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import { CheckBoxOutlined, IndeterminateCheckBoxOutlined } from '@material-ui/icons'

const primaryColor = '#42a5f5'
const secondaryColor = '#9ccc65'
const white = '#fff'
const fontFamily =
  'Lato,"Open Sans","Helvetica",Helvetica,Arial,"Microsoft JhengHei",JhengHei,"微软正黑体", "Heiti TC", "黑體-繁", "Microsoft YaHei New","Microsoft Yahei","微软雅黑",STXihei,SimHei,"华文细黑",sans-serif' // eslint-disable-line

const common = {
  typography: {
    fontFamily,
    button: {
      textTransform: '',
    },
  },
  shape: { borderRadius: 2 },
  props: {
    MuiCheckbox: { color: 'primary', checkedIcon: <CheckBoxOutlined />, indeterminateIcon: <IndeterminateCheckBoxOutlined /> },
    MuiTabs: { indicatorColor: 'primary' },
    MuiSwitch: { color: 'primary' },
  },
}

// ref to https://material-ui-next.com/customization/theme-default/
export const mainThemeOption = {
  ...common,
  palette: {
    // light = ('#7bc0f8',dark:'#3393f2')  secondary = (light:'#badb93',dark:'#8ac053')
    primary: { main: primaryColor, contrastText: white },
    secondary: { main: secondaryColor, contrastText: white },
    grey: { 100: white },
    background: { default: 'transparent' },
  },
  overrides: {
    MuiButton: { flat: { backgroundColor: '#f5f5f5' } },
    MuiTableCell: { head: { backgroundColor: '#eeeeee', borderBottom: '1px solid #aaa', zIndex: 1, position: 'sticky', top: 0 } },
    MuiTablePagination: { root: { borderBottomColor: 'transparent' } },
    MuiTabs: { indicator: { height: 4 }, root: { borderBottom: '1px solid rgb(221, 221, 221)' } },
    MuiCardActions: { root: { justifyContent: 'flex-end' } },
  },
}

const mainTheme = createMuiTheme(mainThemeOption)
export const MainTheme = ({ children }) => <MuiThemeProvider theme={mainTheme}>{children}</MuiThemeProvider>

const darkTheme = createMuiTheme({
  ...common,
  palette: {
    type: 'dark',
    primary: { main: primaryColor },
    secondary: { main: secondaryColor },
    grey: { 900: 'rgba(33, 44, 49, 0.98)' },
  },
})
export const DarkTheme = ({ children }) => <MuiThemeProvider theme={darkTheme}>{children}</MuiThemeProvider>

const datePickerTheme = createMuiTheme({
  ...mainThemeOption,
  overrides: {
    MuiButton: { root: { backgroundColor: null } },
  },
})
export const DatePickerTheme = ({ children }) => <MuiThemeProvider theme={datePickerTheme}>{children}</MuiThemeProvider>
