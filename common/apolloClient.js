import ApolloClient from 'apollo-client'
import { BatchHttpLink } from 'apollo-link-batch-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

const RealHttpLink = process.env.NODE_ENV === 'production' ? BatchHttpLink : require('apollo-link-http').HttpLink
// const RealHttpLink = BatchHttpLink

const createApolloClient = fetch => {
  return new ApolloClient({
    cache: new InMemoryCache(),
    link: new RealHttpLink({
      batchMax: 100, // mutation and query must be serial (in-one-batch). default batchMax=10, some query drop out and run parallelly
      fetch,
    }),
  })
}

export default createApolloClient
