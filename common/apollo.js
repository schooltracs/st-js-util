import _ from 'lodash'
import { useRef } from 'react'
import { useQuery as useQueryOri } from '@apollo/react-hooks'
import gqlTag from 'graphql-tag'

// ----------------------------------------------------------------------------------------------------------
// utils
// ----------------------------------------------------------------------------------------------------------

// const gqlName = gqlAst => {
//   const definition = gqlAst.definitions[0]
//   const selection = _.get(definition, ['selectionSet', 'selections', 0])
//   return selection.alias?.value || selection.name?.value
// }
const gqlVarNames = gqlAst => _.map(gqlAst.definitions[0].variableDefinitions, def => _.get(def, ['variable', 'name', 'value']))
const pickGqlVars = (gqlAst, variables) => _.pick(variables, gqlVarNames(gqlAst))

const calcQueryOption = (gqlAst, variables, option) => {
  // apollo default fetchPolicy: 'cache-first',
  return { fetchPolicy: 'cache-and-network', skip: variables === null, ...option, variables: pickGqlVars(gqlAst, variables) }
}

const not_ = k => k[0] !== '_'
const getOne = obj => {
  if (!obj) return obj
  const type = obj.__typename
  if (type && type[0] === type[0].toUpperCase()) return obj
  if (!_.isPlainObject(obj)) return obj
  const keys = Object.keys(obj)
  const firstKey = _.find(keys, not_)
  const lastKey = _.findLast(keys, not_)
  return firstKey === lastKey ? getOne(obj[firstKey]) : obj
}

const removePrefix = _msg => {
  let msg = _msg
  if (_.startsWith(msg, 'GraphQL error: ')) {
    msg = msg.substr('GraphQL error: '.length)
  }
  const m = msg.match(/^([A-Z]\w+): /)
  if (m) {
    msg = msg.substr(m[0].length)
  }
  return msg
}

// ----------------------------------------------------------------------------------------------------------
// gql
// ----------------------------------------------------------------------------------------------------------

const _memGql = _.memoize(gqlStr => gqlTag(gqlStr))
export const gql = gqlStr => _memGql(gqlStr)

// ----------------------------------------------------------------------------------------------------------
// useQuery
// ----------------------------------------------------------------------------------------------------------

export const useQuery = (gqlAst, option) => {
  const result = useQueryOri(gqlAst, option)
  const duplicateErrRef = useRef()
  if (result.error && duplicateErrRef.current !== result.error) {
    duplicateErrRef.current = result.error
    if (!option.skipErrorAlert && !_.get(result.error, ['networkError', 'skipErrorAlert'])) {
      const errMsg = removePrefix(result.error.message)
      alert(errMsg)
    }
    if (process.env.NODE_ENV !== 'production') {
      console.error(`withQuery: ${gqlAst.loc.source.body}`, result.error)
    }
  }
  return result
}

export const useNetwork = (gqlAst, variables, option) => useQuery(gqlAst, calcQueryOption(gqlAst, variables, option))

export const useOne = (gqlAst, variables, option) => getOne(useNetwork(gqlAst, variables, option).data)

export const useOneWithLoading = (gqlAst, variables, option) => {
  const { data, loading, error } = useNetwork(gqlAst, variables, option)
  return { data: getOne(data), loading, error }
}
