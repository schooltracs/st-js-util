import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import { Router } from 'react-router-dom'
import { CssBaseline } from '@material-ui/core'

import getAppHistory from 'st-js-util/common/appHistory'
import { MainTheme } from 'st-js-util/common/MuiTheme'

const BrowserProviders = ({ apolloClient, children }) => (
  <Router history={getAppHistory()}>
    <ApolloProvider client={apolloClient}>
      <MainTheme>
        <CssBaseline />
        {/* <MuiPickersUtilsProvider utils={LuxonUtils}></MuiPickersUtilsProvider> */}
        {children}
      </MainTheme>
    </ApolloProvider>
  </Router>
)

export default BrowserProviders
